# Extract capacitor voltage rating from PCB EDIF netlist

## Purpose

This script enumerates the capacitors of a PCB that has been designed
using Altium and the CERN component library by parsing the generated
EDIF netlist. An attempt is made to extract the voltage of each power
rail from its name (e.g. P3V3, P12V) and a dictionary setting voltages
to otherwise-named nets can be provided by the user (in a very crude
way, inside the script).

The output of this script is a table that juxtaposes the voltage
rating of the design's capacitors with their actual voltage applied,
easily pointing out if any capacitors of insufficient voltage rating
are used.

### Generate EDIF netlist

From your Altium schematics, click on
Design > Netlist For Project > EDIF For PCB

**Best to use the `-s` switch with this script to sanitize the
generated netlist.**

## Features

The following features are supported:

- voltages are automatically recognized from a wide variety of naming
  conventions (e.g. GND, VSS, P3V3, P12V, 3V3P, 5V0N, +15V,
  +13.1V_WHATEVER, etc.);
- the capacitor voltage ratings are extracted from the library
  definitions;
- the user can provide a list of regular expressions (e.g.
  `.*DP_C2M.*`) in a file in order to ignore nets;
- the user can provide the voltages that can't be automatically
  extracted using a file with statements such as `VREF1: 2.5`;
- the netlist can be sanitized regarding invalid characters and
  non-standard EDIF;
- the script can return a pin count for all ICs; and
- an OpenDocument spreadsheet with the results can be produced.

## Installing

Version 1.9.0 (and above) of the
[spydrnet](https://github.com/byuccl/spydrnet) library has to be
installed (one can use pip to do that).

### Dependencies

* spydrnet
* pandas
* tabulate

## Screenshots

![](screenshot.png)
